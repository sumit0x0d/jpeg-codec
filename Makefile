CC = gcc

CFLAGS = -g # -Wall -Wpedantic -Wextra
# -lgstreamer-1.0 -lgobject-2.0 -lglib-2.0

all:
	$(CC) main.c -o jpec-codec \
	turbojpeg/turbojpeg.c \
	$(CFLAGS) $(shell pkg-config --cflags --libs libturbojpeg)

clean:
	rm wayland-client
