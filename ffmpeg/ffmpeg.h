#ifndef JPEG_CODEC_FFMPEG_H
#define JPEG_CODEC_FFMPEG_H

#include <stddef.h>
#include <stdint.h>

typedef struct {
} JC_Ffmpeg;

JC_Ffmpeg *JC_Ffmpeg_Create(uint8_t *buffer, size_t size);
void JC_Ffmpeg_Destroy(JC_Ffmpeg *ffmpeg);

void JC_Ffmpeg_Encode(JC_Ffmpeg *ffmpeg);
void JC_Ffmpeg_Dncode(JC_Ffmpeg *ffmpeg);

#endif
