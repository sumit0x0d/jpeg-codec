#ifndef JPEC_CODEC_MEDIASDK_H
#define JPEC_CODEC_MEDIASDK_H

#include <mfx/mfxvideo.h>
#include <va/va.h>

typedef struct {
	mfxSession session;
	mfxVersion version;
	mfxBitstream bitstream;
	mfxVideoParam videoParamIn;
	mfxVideoParam videoParamOut;
	mfxFrameSurface1 *frameSurface1In;
	mfxFrameSurface1 *frameSurface1Out;
	mfxU8 *frameBuffer;
	mfxIMPL impl;
	mfxFrameAllocRequest frameAllocRequest;
	VADisplay vaDisplay;
	void *sharedMemory;
} JC_Mediasdk;

JC_Mediasdk *JC_Mediasdk_Create(uint8_t *buffer, size_t size);
void JC_Mediasdk_Destroy(JC_Mediasdk *mediasdk);

void JC_Mediasdk_Encode(JC_Mediasdk *mediasdk);
void JC_Mediasdk_Dncode(JC_Mediasdk *mediasdk);

#endif
