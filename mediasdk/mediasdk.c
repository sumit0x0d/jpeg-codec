#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

#include <sys/stat.h>
#include <turbojpeg.h>

#include <va/va.h>
#include <va/va_drm.h>

#include <sys/shm.h>

#include <fcntl.h>
#include <unistd.h>

#include <mfx/mfxdefs.h>
#include <mfx/mfxvideo.h>
#include <mfx/mfxjpeg.h>

#define JPEG_BPP 32
#define JPEG_WIDTH 1920
#define JPEG_HEIGHT 1080
#define PIXEL_FORMAT TJPF_BGRA
#define JPEG_SUBSAMP TJSAMP_444
#define JPEG_QUAL 90
#define BUFFER_SIZE JPEG_WIDTH * JPEG_HEIGHT * JPEG_BPP / 8

#define TEST_COUNT 60000
#define FPS 1000

typedef struct Jpeg {
	unsigned char *buf;
	unsigned long size;
	int width;
	int height;
	int pixelFormat;
	int subsamp;
	int qual;
} Jpeg;

Jpeg *Jpeg_Create(int width, int height, int pixelFormat, int subsamp, int qual)
{
	Jpeg *jpeg  = (Jpeg *)malloc(sizeof (Jpeg));
	assert(jpeg);
	unsigned char *srcBuf = (unsigned char *)calloc(height, width * JPEG_BPP / 8);
	assert(srcBuf);
	for (size_t i = 0; i < BUFFER_SIZE; i++) {
		srcBuf[i] = rand();
	}
	tjhandle handle = tjInitCompress();
	assert(handle);
	int status = tjCompress2(handle, srcBuf, width, 0, height, pixelFormat, &jpeg->buf, &jpeg->size, subsamp, qual,
		0);
	assert(!status);
	jpeg->width = width;
	jpeg->height = height;
	jpeg->pixelFormat = pixelFormat; 
	jpeg->subsamp = subsamp;
	jpeg->qual = qual;
	free(srcBuf);
	tjDestroy(handle);
	return jpeg;
}

void Jpeg_Save(char *filename, unsigned char *buf, unsigned long size)
{
	FILE *file = fopen(filename, "wb");
	assert(file);
	fwrite(buf, 1, size, file);
	fclose(file);
}

typedef struct Mfx {
	mfxSession session;
	mfxVersion version;
	mfxBitstream bitstream;
	mfxVideoParam videoParamIn;
	mfxVideoParam videoParamOut;
	mfxFrameSurface1 *frameSurface1In;
	mfxFrameSurface1 *frameSurface1Out;
	mfxU8 *frameBuffer;
	mfxIMPL impl;
	mfxFrameAllocRequest frameAllocRequest;
	VADisplay vaDisplay;
	void *sharedMemory;
} Mfx;

mfxU16 getFreeSurfaceIndex(mfxFrameSurface1 **frameSurface1, mfxU16 numFrame)
{
	if (frameSurface1) {
		for (mfxU16 i = 0; i < numFrame; i++) {
			if (frameSurface1[i]->Data.Locked == 0) {
				return i;
			}
		}
	}
	return MFX_ERR_NOT_FOUND;
}

void Mfx_WriteSharedMemory(mfxFrameSurface1 *frameSurface1Out, void *sharedMemory)
{
	mfxFrameInfo *pInfo = &frameSurface1Out->Info;
	mfxFrameData *pData = &frameSurface1Out->Data;
	mfxU16 height, width, pitch;
	mfxU8 *ptr;
	width = pInfo->CropW;
	height = pInfo->CropH;
	pitch = pData->Pitch;
	ptr = pData->B;
	for (mfxU16 i = 0; i < height; i++) {
		memcpy(sharedMemory, ptr + i * pitch, width * JPEG_BPP / 8);
		sharedMemory = (char *)sharedMemory + (width * JPEG_BPP / 8);
	}
}

void saveToFile(void *sharedMemory)
{
	FILE *file = fopen("buffer", "w");
	assert(file);
	fwrite(sharedMemory, 1, BUFFER_SIZE, file);
	fclose(file);
}

Mfx *Mfx_Create(uint8_t *buf, size_t size)
{
	Mfx *mfx = (Mfx *)malloc(sizeof (Mfx));
	assert(mfx);

	mfx->session = NULL;
	mfx->version.Major = MFX_VERSION_MAJOR;
	mfx->version.Minor = MFX_VERSION_MINOR;
	mfx->impl = MFX_IMPL_HARDWARE;

	memset(&mfx->bitstream, 0, sizeof (mfxBitstream));
	mfx->bitstream.MaxLength = JPEG_WIDTH * JPEG_HEIGHT * JPEG_BPP / 8;
	mfx->bitstream.Data = (mfxU8 *)malloc(mfx->bitstream.MaxLength);
	assert(mfx->bitstream.Data);
	memset(&mfx->videoParamIn, 0, sizeof (mfxVideoParam));
	mfx->videoParamIn.mfx.CodecId = MFX_CODEC_JPEG;
	mfx->videoParamIn.IOPattern = MFX_IOPATTERN_OUT_SYSTEM_MEMORY;

	memset(&mfx->videoParamOut, 0, sizeof (mfxVideoParam));
	mfx->videoParamOut.mfx.CodecId = MFX_CODEC_JPEG;

	memset(&mfx->frameAllocRequest, 0, sizeof (mfxFrameAllocRequest));

	mfx->frameSurface1Out = NULL;

	int majorVersion = VA_MAJOR_VERSION, minorVersion = VA_MINOR_VERSION;
	int card = open("/dev/dri/card0", O_RDWR);
	assert(card >= 0);
	mfx->vaDisplay = vaGetDisplayDRM(card);
	assert(vaInitialize(mfx->vaDisplay, &majorVersion, &minorVersion) == 0);

	mfxStatus status = MFX_ERR_NONE;

	mfx->bitstream.Data = buf;
	mfx->bitstream.DataOffset = 0;
	mfx->bitstream.DataLength = size;
	mfx->bitstream.MaxLength = JPEG_HEIGHT * JPEG_HEIGHT * JPEG_BPP / 8;

	status = MFXInit(mfx->impl, &mfx->version, &mfx->session);
	if (status < 0 || status == MFX_WRN_PARTIAL_ACCELERATION) {
		printf("[MFX] ERROR: MFXInit() %d\n", status);
		return NULL;
	} else {
		printf("[MFX] INFO: MFXInit() %d\n", status);
	}

	status = MFXQueryIMPL(mfx->session, &mfx->impl);
	if (status < 0 || status == MFX_WRN_PARTIAL_ACCELERATION) {
		printf("[MFX] ERROR: MFXQueryIMPL() %d\n", status);
		return NULL;
	} else {
		printf("[MFX] INFO: MFXQueryIMPL() %d\n", status);
	}

	status = MFXQueryVersion(mfx->session, &mfx->version);
	if (status < 0 || status == MFX_WRN_PARTIAL_ACCELERATION) {
		printf("[MFX] ERROR: MFXQueryVersion() %d\n", status);
		return NULL;
	} else {
		printf("[MFX] INFO: MFXQueryVersion() %d\n", status);
	}

	status = MFXVideoCORE_SetHandle(mfx->session, MFX_HANDLE_VA_DISPLAY, (mfxHDL)mfx->vaDisplay);
	if (status < 0 || status == MFX_WRN_PARTIAL_ACCELERATION) {
		printf("[MFX] ERROR: MFXVideoCORE_SetHandle() %d\n", status);
		return NULL;
	} else {
		printf("[MFX] INFO: MFXVideoCORE_SetHandle() %d\n", status);
	}

	status = MFXVideoDECODE_DecodeHeader(mfx->session, &mfx->bitstream, &mfx->videoParamIn);
	if (status < 0 || status == MFX_WRN_PARTIAL_ACCELERATION) {
		printf("[MFX] ERROR: MFXVideoDECODE_DecodeHeader() %d\n", status);
		return NULL;
	} else {
		printf("[MFX] INFO: MFXVideoDECODE_DecodeHeader() %d\n", status);
	}

	mfx->videoParamIn.mfx.FrameInfo.FourCC = MFX_FOURCC_RGB4;
	mfx->videoParamIn.mfx.FrameInfo.ChromaFormat = MFX_CHROMAFORMAT_YUV444;

	status = MFXVideoDECODE_Query(mfx->session, &mfx->videoParamIn, &mfx->videoParamOut);
	if (status < 0 || status == MFX_WRN_PARTIAL_ACCELERATION) {
		printf("[MFX] ERROR: MFXVideoDECODE_Query() %d\n", status);
		return NULL;
	} else {
		printf("[MFX] INFO: MFXVideoDECODE_Query() %d\n", status);
	}

	status = MFXVideoDECODE_QueryIOSurf(mfx->session, &mfx->videoParamIn, &mfx->frameAllocRequest);
	if (status < 0 || status == MFX_WRN_PARTIAL_ACCELERATION) {
		printf("[MFX] ERROR: MFXVideoDECODE_QueryIOSurf() %d\n", status);
		return NULL;
	} else {
		printf("[MFX] INFO: MFXVideoDECODE_QueryIOSurf() %d\n", status);
	}

	mfxU16 numFrameSuggested = mfx->frameAllocRequest.NumFrameSuggested;
	mfxU16 infoWidth = mfx->frameAllocRequest.Info.Width;
	mfxU16 infoHeight = mfx->frameAllocRequest.Info.Height;
	mfxU32 surfaceSize = infoWidth * infoHeight * JPEG_BPP / 8;

	mfxU8 *frameBuffer = (mfxU8*)malloc(numFrameSuggested * surfaceSize);
	assert(frameBuffer);
	mfx->frameSurface1In = (mfxFrameSurface1*)malloc(numFrameSuggested * sizeof (mfxFrameSurface1));
	assert(mfx->frameSurface1In);
	mfx->sharedMemory = malloc(JPEG_WIDTH * JPEG_HEIGHT * JPEG_BPP / 8);
	assert(mfx->sharedMemory);

	for (int i = 0; i < numFrameSuggested; i++) {
		memset(&mfx->frameSurface1In[i], 0, sizeof(mfxFrameSurface1));
		memcpy(&mfx->frameSurface1In[i].Info, &mfx->videoParamIn.mfx.FrameInfo, sizeof(mfxFrameInfo));
		mfx->frameSurface1In[i].Data.B = &frameBuffer[surfaceSize * i];
		mfx->frameSurface1In[i].Data.G = mfx->frameSurface1In[i].Data.B + 1;
		mfx->frameSurface1In[i].Data.R = mfx->frameSurface1In[i].Data.B + 2;
		mfx->frameSurface1In[i].Data.A = mfx->frameSurface1In[i].Data.B + 3;
		mfx->frameSurface1In[i].Data.Pitch = infoWidth * JPEG_BPP / 8;
	}

	status = MFXVideoDECODE_Init(mfx->session, &mfx->videoParamIn);
	if (status < 0 || status == MFX_WRN_PARTIAL_ACCELERATION) {
		printf("[MFX] ERROR: MFXVideoDECODE_Init() %d\n", status);
		return NULL;
	} else {
		printf("[MFX] INFO: MFXVideoDECODE_Init() %d\n", status);
	}

	mfxSyncPoint syncp;
	memset(&syncp, 0, sizeof(mfxSyncPoint));
	mfxU16 index = getFreeSurfaceIndex(&mfx->frameSurface1In, numFrameSuggested);
	for (;;) {
		status = MFXVideoDECODE_DecodeFrameAsync(mfx->session, &mfx->bitstream, &mfx->frameSurface1In[index],
			&mfx->frameSurface1Out, &syncp);
		if (status < MFX_ERR_NONE) {
			printf("[MFX] ERROR: MFXVideoCORE_DecodeFrameAsync() %d\n", status);
			return NULL;
		}
		if (status == MFX_WRN_DEVICE_BUSY) {
			sleep(1);
		} else {
			break;
		}
	}
	if (status == MFX_ERR_NONE && syncp) {
		status = MFXVideoCORE_SyncOperation(mfx->session, syncp, 60000);
		if (status < MFX_ERR_NONE) {
			printf("[MFX] ERROR: MFXVideoCORE_SyncOperation() %d\n", status);
			return NULL;
		} else {
			printf("[MFX] INFO: MFXVideoCORE_SyncOperation() %d\n", status);
		}
	}

	status = MFXVideoDECODE_Close(mfx->session);
	if (status < MFX_ERR_NONE) {
		printf("[MFX] ERROR: MFXVideoDECODE_Close() %d\n", status);
	} else {
		printf("[MFX] INFO: MFXVideoDECODE_Close() %d\n", status);
	}

	free(frameBuffer);
	return mfx;
}

void Mfx_DecodeFrameWithHeader(Mfx *mfx, unsigned char *buf, unsigned long size, mfxU16 numFrameSuggested,
	mfxU32 surfaceSize, mfxU16 infoWidth)
{
	mfxSyncPoint syncp;
	memset(&syncp, 0, sizeof(mfxSyncPoint));
	mfxU16 index = 0;
	mfxStatus status = MFX_ERR_NONE;
	mfx->bitstream.Data = buf;
	mfx->bitstream.DataOffset = 0;
	mfx->bitstream.DataLength = size;
	mfx->bitstream.MaxLength = JPEG_WIDTH * JPEG_HEIGHT * JPEG_BPP / 8;

	status = MFXVideoDECODE_DecodeHeader(mfx->session, &mfx->bitstream, &mfx->videoParamIn);
	if (status < 0) {
		printf("[MFX] ERROR: MFXVideoDECODE_DecodeHeader() %d\n", status);
		exit(0);
	// } else {
	// 	printf("[MFX] INFO: MFXVideoDECODE_DecodeHeader() %d\n", status);
	}

	mfx->videoParamIn.mfx.FrameInfo.FourCC = MFX_FOURCC_RGB4;
	mfx->videoParamIn.mfx.FrameInfo.ChromaFormat = MFX_CHROMAFORMAT_YUV444;

	status = MFXVideoDECODE_Query(mfx->session, &mfx->videoParamIn, &mfx->videoParamOut);
	if (status < 0) {
		printf("[MFX] ERROR: MFXVideoDECODE_Query() %d\n", status);
		exit(0);
	// } else {
	// 	printf("[MFX] INFO: MFXVideoDECODE_Query() %d\n", status);
	}

	// status = MFXVideoDECODE_QueryIOSurf(mfx->session, &mfx->videoParamIn, &mfx->frameAllocRequest);
	// if (status < 0 || status == MFX_WRN_PARTIAL_ACCELERATION) {
	// 	printf("[MFX] ERROR: MFXVideoDECODE_QueryIOSurf() %d\n", status);
	// 	return -1;
	// } else {
	// 	printf("[MFX] INFO: MFXVideoDECODE_QueryIOSurf() %d\n", status);
	// }

	// mfxU16 numFrameSuggested = mfx->frameAllocRequest.NumFrameSuggested;
	// mfxU16 infoWidth = mfx->frameAllocRequest.Info.Width;
	// mfxU16 infoHeight = mfx->frameAllocRequest.Info.Height;
	// mfxU32 surfaceSize = infoWidth * infoHeight * JPEG_BPP / 8;

	mfxU8 *frameBuffer = (mfxU8 *)malloc(numFrameSuggested * surfaceSize);
	assert(frameBuffer);
	// mfxFrameSurface1 *frameSurface1In = (mfxFrameSurface1*)malloc(numFrameSuggested * sizeof (mfxFrameSurface1));
	// assert(frameSurface1In);
	// mfxU8 *frameBuffer = buf;

	// printf("%p, %p, frameBuffer : %p\n", mfx->frameSurface1Out, *mfx->frameSurface1Out, frameBuffer);
	for (int i = 0; i < numFrameSuggested; i++) {
		memset(&mfx->frameSurface1In[i], 0, sizeof(mfxFrameSurface1));
		memcpy(&mfx->frameSurface1In[i].Info, &mfx->videoParamIn.mfx.FrameInfo, sizeof(mfxFrameInfo));
		mfx->frameSurface1In[i].Data.B = &frameBuffer[surfaceSize * i];
		mfx->frameSurface1In[i].Data.G = mfx->frameSurface1In[i].Data.B + 1;
		mfx->frameSurface1In[i].Data.R = mfx->frameSurface1In[i].Data.B + 2;
		mfx->frameSurface1In[i].Data.A = mfx->frameSurface1In[i].Data.B + 3;
		mfx->frameSurface1In[i].Data.Pitch = infoWidth * JPEG_BPP / 8;
	}

	status = MFXVideoDECODE_Init(mfx->session, &mfx->videoParamIn);
	if (status < 0) {
		printf("[MFX] ERROR: MFXVideoDECODE_Init() %d\n", status);
		exit(0);
	// } else {
	// 	printf("[MFX] INFO: MFXVideoDECODE_Init() %d\n", status);
	}

	index = getFreeSurfaceIndex(&mfx->frameSurface1In, numFrameSuggested);
	for (;;) {
		status = MFXVideoDECODE_DecodeFrameAsync(mfx->session, &mfx->bitstream, &mfx->frameSurface1In[index],
			&mfx->frameSurface1Out, &syncp);
		if (status < MFX_ERR_NONE) {
			printf("[MFX] ERROR: MFXVideoDECODE_Init() %d\n", status);
			exit(0);
		} else if (status == MFX_WRN_DEVICE_BUSY) {
			sleep(1);
		} else {
			break;
		}
	}
	if (status == MFX_ERR_NONE && syncp) {
		status = MFXVideoCORE_SyncOperation(mfx->session, syncp, 60000);
		if (status < MFX_ERR_NONE) {
			printf("[MFX] ERROR: MFXVideoCORE_SyncOperation() %d\n", status);
			exit(0);
		// } else {
		// 	printf("[MFX] INFO: MFXVideoCORE_SyncOperation() %d\n", status);
		}
	}
	// index = getFreeSurfaceIndex(&frameSurface1In, NumFrameSuggested);
	// for (;;) {
	// 	status = MFXVideoDECODE_DecodeFrameAsync(mfx->session, &mfx->bitstream, &frameSurface1In[index],
	// 		mfx->frameSurface1Out, &syncp);
	// 	if (status == MFX_WRN_DEVICE_BUSY) {
	// 		sleep(1);
	// 	} else {
	// 		break;
	// 	}
	// }
	// if (status == MFX_ERR_NONE && syncp) {
	// 	status = MFXVideoCORE_SyncOperation(mfx->session, syncp, 60000);
	// 	if (status < MFX_ERR_NONE) {
	// 		printf("[MFX] ERROR: MFXVideoCORE_SyncOperation() %d\n", status);
	// 	} else {
	// 		printf("[MFX] INFO: MFXVideoCORE_SyncOperation() %d\n", status);
	// 	}
	// 	// Mfx_WriteSharedMemory(*mfx->frameSurface1Out, mfx->sharedMemory);
	// }
	// if (status < 0) {
	// 	printf("STATUS: %d\n", status);
	// 	return -1;
	// }

	status = MFXVideoDECODE_Close(mfx->session);
	if (status < MFX_ERR_NONE) {
		printf("[MFX] ERROR: MFXVideoDECODE_Close() %d\n", status);
		exit(0);
	// } else {
	// 	printf("[MFX] INFO: MFXVideoDECODE_Close() %d\n", status);
	}

	free(frameBuffer);
}

void Mfx_DecodeFrame(Mfx *mfx, mfxU16 numFrameSuggested)
{
	mfxSyncPoint syncp;
	memset(&syncp, 0, sizeof(mfxSyncPoint));
	mfxU16 index = 0;
	mfxStatus status = MFX_ERR_NONE;

	index = getFreeSurfaceIndex(&mfx->frameSurface1In, numFrameSuggested);
	for (;;) {
		status = MFXVideoDECODE_DecodeFrameAsync(mfx->session, &mfx->bitstream, &mfx->frameSurface1In[index],
			&mfx->frameSurface1Out, &syncp);
		if (status < MFX_ERR_NONE) {
			printf("[MFX] ERROR: MFXVideoDECODE_Init() %d\n", status);
			exit(0);
		} else if (status == MFX_WRN_DEVICE_BUSY) {
			sleep(1);
		} else {
			break;
		}
	}
	if (status == MFX_ERR_NONE && syncp) {
		status = MFXVideoCORE_SyncOperation(mfx->session, syncp, 60000);
		if (status < MFX_ERR_NONE) {
			printf("[MFX] ERROR: MFXVideoCORE_SyncOperation() %d\n", status);
			exit(0);
		} else {
			Mfx_WriteSharedMemory(mfx->frameSurface1Out, mfx->sharedMemory);
			// saveToFile(mfx->sharedMemory);
			// exit(0);
			// printf("[MFX] INFO: MFXVideoCORE_SyncOperation() %d\n", status);
		}
	}

}

int main(int argc, char *argv[])
{
	if (argc < 1) {
		printf("USAGE: ./decoder frame1 frame2\n");
		return -1;
	}

	char val[8];
	sprintf(val, "%ub", 1);
	int err = setenv("TJ_RESTART", val, 1);
	if ( err < 0) {
		printf("[ERROR]");
		return err;
	}

	int status;
	struct timespec timeBefore, timeAfter;
	// struct timespec timeFps = {0, 1e9 / FPS};
	// unsigned long time60Fps = 1000 / FPS;
	unsigned long timeAvg = 0;
	unsigned long timeDiff[TEST_COUNT];
	// struct timespec timespecMfx[TEST_COUNT];
	FILE* file1 = fopen(argv[1], "rb");
	assert(file1);
	// FILE* file2 = fopen(argv[2], "rb");
	// assert(file2);

	uint8_t *buf1 = (uint8_t *)malloc(JPEG_WIDTH * JPEG_HEIGHT * JPEG_BPP / 8);
	size_t size1 = fread(buf1, 1, JPEG_WIDTH * JPEG_HEIGHT * JPEG_BPP / 8, file1);
	// uint8_t *buf2 = (uint8_t*)malloc(JPEG_WIDTH * JPEG_HEIGHT * JPEG_BPP / 8);
	// size_t size2 = fread(buf2, 1, JPEG_WIDTH * JPEG_HEIGHT * JPEG_BPP / 8, file2);

	Jpeg *jpeg = Jpeg_Create(JPEG_WIDTH, JPEG_HEIGHT, PIXEL_FORMAT, JPEG_SUBSAMP, JPEG_QUAL);
	// Jpeg_Save("test.jpg", jpeg->buf, jpeg->size);
	// exit(0);
	Mfx *mfx = Mfx_Create(jpeg->buf, jpeg->size);
	if (!mfx) {
		printf("Cant create MFX object\n");
		exit(0);
	}

	mfx->bitstream.Data = buf1;
	mfx->bitstream.DataOffset = 0;
	mfx->bitstream.DataLength = size1;
	mfx->bitstream.MaxLength = JPEG_WIDTH * JPEG_HEIGHT * JPEG_BPP / 8;

	status = MFXVideoDECODE_DecodeHeader(mfx->session, &mfx->bitstream, &mfx->videoParamIn);
	if (status < 0) {
		printf("[MFX] ERROR: MFXVideoDECODE_DecodeHeader() %d\n", status);
		exit(0);
	} else {
		printf("[MFX] INFO: MFXVideoDECODE_DecodeHeader() %d\n", status);
	}

	mfx->videoParamIn.mfx.FrameInfo.FourCC = MFX_FOURCC_RGB4;
	mfx->videoParamIn.mfx.FrameInfo.ChromaFormat = MFX_CHROMAFORMAT_YUV444;

	status = MFXVideoDECODE_Query(mfx->session, &mfx->videoParamIn, &mfx->videoParamOut);
	if (status < 0) {
		printf("[MFX] ERROR: MFXVideoDECODE_Query() %d\n", status);
		exit(0);
	} else {
		printf("[MFX] INFO: MFXVideoDECODE_Query() %d\n", status);
	}

	status = MFXVideoDECODE_QueryIOSurf(mfx->session, &mfx->videoParamIn, &mfx->frameAllocRequest);
	if (status < 0 || status == MFX_WRN_PARTIAL_ACCELERATION) {
		printf("[MFX] ERROR: MFXVideoDECODE_QueryIOSurf() %d\n", status);
		exit(0);
	} else {
		printf("[MFX] INFO: MFXVideoDECODE_QueryIOSurf() %d\n", status);
	}

	mfxU16 numFrameSuggested = mfx->frameAllocRequest.NumFrameSuggested;
	mfxU16 infoWidth = mfx->frameAllocRequest.Info.Width;
	mfxU16 infoHeight = mfx->frameAllocRequest.Info.Height;
	mfxU32 surfaceSize = infoWidth * infoHeight * JPEG_BPP / 8;

	mfx->frameBuffer = (mfxU8*)malloc(numFrameSuggested * surfaceSize);
	assert(mfx->frameBuffer);

	for (int i = 0; i < numFrameSuggested; i++) {
		memset(&mfx->frameSurface1In[i], 0, sizeof(mfxFrameSurface1));
		memcpy(&mfx->frameSurface1In[i].Info, &mfx->videoParamIn.mfx.FrameInfo, sizeof(mfxFrameInfo));
		mfx->frameSurface1In[i].Data.B = &mfx->frameBuffer[surfaceSize * i];
		mfx->frameSurface1In[i].Data.G = mfx->frameSurface1In[i].Data.B + 1;
		mfx->frameSurface1In[i].Data.R = mfx->frameSurface1In[i].Data.B + 2;
		mfx->frameSurface1In[i].Data.A = mfx->frameSurface1In[i].Data.B + 3;
		mfx->frameSurface1In[i].Data.Pitch = infoWidth * JPEG_BPP / 8;
	}

	status = MFXVideoDECODE_Init(mfx->session, &mfx->videoParamIn);
	if (status < 0) {
		printf("[MFX] ERROR: MFXVideoDECODE_Init() %d\n", status);
		exit(0);
	} else {
		printf("[MFX] INFO: MFXVideoDECODE_Init() %d\n", status);
	}

	int frameCount = 0;
	int frameDelay = NSEC_SEC/FPS;
	int overSlept = 0;
	clock_gettime(1, &timeBefore);
	static struct timespec ts;
	ardClockGettime(1, &ts);
	static struct timespec timeStart;
	ardClockGettime(1, &timeStart);
	int seconds = 0;
	int totalFrames = 0;

#if 0
	for (int i = 0; i < TEST_COUNT; i++) {
		clock_gettime(1, &timeBefore);
		mfx->bitstream.Data = buf1;
		mfx->bitstream.DataOffset = 0;
		mfx->bitstream.DataLength = size1;
		Mfx_DecodeFrame(mfx, numFrameSuggested);
		clock_gettime(1, &timeAfter);
		if (overSlept < frameDelay/NSEC_USEC) {
			overSlept = ardNanoSleep(1, &ts, frameDelay/NSEC_USEC);
		} else {
			overSlept -= frameDelay/NSEC_USEC;
			ardClockAddMicros(&ts, frameDelay/NSEC_USEC);
		}
		timeDiff[i] =
			((timeAfter.tv_sec * 1e9) + timeAfter.tv_nsec) -
			((timeBefore.tv_sec * 1e9) + timeBefore.tv_nsec);
		long tDiff =
			((timeAfter.tv_sec * 1e9) + timeAfter.tv_nsec) - ((timeStart.tv_sec * 1e9) + timeStart.tv_nsec);
		if (tDiff >= 1e9) {
			clock_gettime(1, &timeStart);
			totalFrames += frameCount;
			frameCount = 0;
			seconds++;
		}
		else {
			frameCount++;
		}
	}
#endif
	status = MFXVideoDECODE_Close(mfx->session);
	if (status < MFX_ERR_NONE) {
		printf("[MFX] ERROR: MFXVideoDECODE_Close() %d\n", status);
		exit(0);
	} else {
		printf("[MFX] INFO: MFXVideoDECODE_Close() %d\n", status);
	}

	int width;
	int height;
	int jpegSubsamp;
	tjhandle handle = tjInitDecompress();

	tjDecompressHeader2(handle, buf1, size1, &width, &height, &jpegSubsamp);
	// free(handle);
	printf("[TJ] width: %d, height: %d, jpegSubsamp: %d\n", width, height, jpegSubsamp);

	// struct timespec timeStart;
	// struct timespec t;
	// clock_gettime(1, &ts);

#if 1
	frameCount = 0;
	frameDelay = NSEC_SEC/FPS;
	overSlept = 0;
	clock_gettime(1, &timeBefore);
	ardClockGettime(1, &ts);
	ardClockGettime(1, &timeStart);
	seconds = 0;
	totalFrames = 0;
	for (int i = 0; i < TEST_COUNT; i++) {
		clock_gettime(1, &timeBefore);
		if (tjDecompress2(handle, (unsigned char*)buf1, size1, mfx->sharedMemory, width, 0, height,
				PIXEL_FORMAT, TJFLAG_FASTUPSAMPLE) == -1) {
			printf("[TJ] ERROR\n");
			exit(0);
		}
		// Mfx_DecodeFrameWithHeader(mfx, buf1, size1, 8, infoHeight * infoWidth * JPEG_BPP / 8, infoWidth);
		clock_gettime(1, &timeAfter);
		if (overSlept < frameDelay/NSEC_USEC) {
			overSlept = ardNanoSleep(1, &ts, frameDelay/NSEC_USEC);
		} else {
			overSlept -= frameDelay/NSEC_USEC;
			ardClockAddMicros(&ts, frameDelay/NSEC_USEC);
		}
		timeDiff[i] =
			((timeAfter.tv_sec * 1e9) + timeAfter.tv_nsec) -
			((timeBefore.tv_sec * 1e9) + timeBefore.tv_nsec);
		long tDiff =
			((timeAfter.tv_sec * 1e9) + timeAfter.tv_nsec) - ((timeStart.tv_sec * 1e9) + timeStart.tv_nsec);
		if (tDiff >= 1e9) {
			clock_gettime(1, &timeStart);
			totalFrames += frameCount;
			frameCount = 0;
			seconds++;
		}
		else {
			frameCount++;
		}
	}
#endif

#if 0
	for (int i = 0; i < TEST_COUNT; i++) {
		// printf("[MFX] Decoding frame\n");
		// clock_gettime(1, &timeBefore);
		// if (tjDecompress2(handle, (unsigned char*)buf1, size1, mfx->sharedMemory, width, 0, height, PIXEL_FORMAT,
		// 		TJFLAG_FASTUPSAMPLE) == -1) {
		// 	printf("[TJ] ERROR\n");
		// 	exit(0);
		// }

		Mfx_DecodeFrameWithHeader(mfx, buf1, size1, 8, infoHeight * infoWidth * JPEG_BPP / 8, infoWidth);
		clock_gettime(1, &timeAfter);
		if (overSlept < frameDelay/NSEC_USEC) {
			overSlept = ardNanoSleep(1, &ts, frameDelay/NSEC_USEC);
		} else {
			overSlept -= frameDelay/NSEC_USEC;
			ardClockAddMicros(&ts, frameDelay/NSEC_USEC);
		}
		timeDiff[i] =
			((timeAfter.tv_sec * 1e9) + timeAfter.tv_nsec) - ((timeBefore.tv_sec * 1e9) + timeBefore.tv_nsec);
		if (timeDiff[i] >= 1e9) {
			printf("Frame processed: %d\n", frameCount);
			clock_gettime(1, &timeBefore);
			frameCount = 0;
		}
		frameCount++;
	}
#endif
	
	// if (shmid == -1) {
	// 	int shmid = shmget(12345, JPEG_WIDTH * JPEG_HEIGHT * JPEG_BPP / 8, IPC_CREAT | 0666);
	// 	printf("shmget failed");
	// 	abort();
	// }

	// mfx->sharedMemory = shmat(shmid, NULL, 0);
	// if (mfx->sharedMemory == (void*)-1) {
	// 	if (shmctl(shmid, IPC_RMID, 0) != 0) {
	// 		printf("shmat failed");
	// 	}
	// 	printf("shmgat failed");
	// 	abort();
	// }

	mfx->bitstream.MaxLength = JPEG_WIDTH * JPEG_HEIGHT * JPEG_BPP / 8;
	mfx->sharedMemory = malloc(JPEG_WIDTH * JPEG_HEIGHT * JPEG_BPP / 8);
	assert(mfx->sharedMemory);


	for (int i = 0; i < TEST_COUNT; i++) {
		timeAvg += (timeDiff[i]/(TEST_COUNT));
	}

	printf("INFO: Frame Rate = %d\n", totalFrames/seconds);
	printf("Average Time: %ld usec\n", timeAvg/1000);

	return 0;
}
