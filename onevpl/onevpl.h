#ifndef MEDIASDK_CONTEXT_H
#define MEDIASDK_CONTEXT_H

#include <vpl/mfxvideo.h>
#include <va/va.h>

typedef struct {
	mfxSession session;
	mfxVersion version;
	mfxBitstream bitstream;
	mfxVideoParam videoParamIn;
	mfxVideoParam videoParamOut;
	mfxFrameSurface1 *frameSurface1In;
	mfxFrameSurface1 *frameSurface1Out;
	mfxU8 *frameBuffer;
	mfxIMPL impl;
	mfxFrameAllocRequest frameAllocRequest;
	VADisplay vaDisplay;
	void *sharedMemory;
} MediasdkContext;

MediasdkContext *MediasdkContext_Create(uint8_t *buffer, size_t size);
void MediasdkContext_Destroy(MediasdkContext *mediasdk);

void MediasdkContext_Encode(MediasdkContext *mediasdk);
void MediasdkContext_Dncode(MediasdkContext *mediasdk);

#endif
