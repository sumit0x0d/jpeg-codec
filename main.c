#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#define JPEG_RAND 1

#define JPEG_BPP 32
#define JPEG_PIXEL_FORMAT TJPF_BGRA
#define JPEG_SUBSAMPLE TJSAMP_444
#define JPEG_QUALITY 90
#define BUFFER_SIZE JPEG_WIDTH * JPEG_HEIGHT * JPEG_BPP / 8

#define FPS 1000

#include "turbojpeg/turbojpeg.h"

static void saveBuffer(char *filename, unsigned char *buffer, unsigned long size)
{
	FILE *file = fopen(filename, "wb");
	assert(file);
	fwrite(buffer, 1, size, file);
	fclose(file);
}

int main(int argc, char *argv[])
{
	unsigned char *jpegBuffer;
	unsigned long jpegSize = 0;

	// if (argc < 3) {
	// 	printf("Usage: ./encoder <width> <height>\n");
	// 	exit(0);
	// }
	char val[8];
	sprintf(val, "%ub", 1);
	int err = setenv("TJ_RESTART", val, 1);
	if ( err < 0) {
		printf("ERROR");
		return err;
	}
	// int width = atoi(argv[1]);
	// int height = atoi(argv[2]);
	// printf("[INFO] Jpeg Resolution: %d x %d\n", width, height);
	int widths[] = {8, 16, 32, 64, 128, 256, 512, 1024, 2048};
	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 9; j++) {
			printf("[INFO] Jpeg Resolution: %d x %d\n", widths[i], widths[j]);
			unsigned char *buffer = malloc(widths[i] * widths[j] * 4);
			for (int k = 0; k < widths[i] * widths[j] * 4; k++) {
				buffer[k] = rand();
			}
			// TJSAMP_444
			JC_Turbojpeg *turbojpeg444 = JC_Turbojpeg_Create(widths[i], widths[j], JPEG_PIXEL_FORMAT, TJSAMP_444,
				JPEG_QUALITY);
			JC_Turbojpeg_Encode(turbojpeg444, buffer, &jpegBuffer, &jpegSize);
			char filename444[64];
			sprintf(filename444, "samples/jpeg-buffers/%dx%d-bgra-444-%d.jpg", widths[i], widths[j], JPEG_QUALITY);
			saveBuffer(filename444, jpegBuffer, jpegSize);

			// TJSAMP_422
			JC_Turbojpeg *turbojpeg422 = JC_Turbojpeg_Create(widths[i], widths[j], JPEG_PIXEL_FORMAT, TJSAMP_422,
				JPEG_QUALITY);
			JC_Turbojpeg_Encode(turbojpeg422, buffer, &jpegBuffer, &jpegSize);
			char filename422[64];
			sprintf(filename422, "samples/jpeg-buffers/%dx%d-bgra-422-%d.jpg", widths[i], widths[j], JPEG_QUALITY);
			saveBuffer(filename422, jpegBuffer, jpegSize);

			// TJSAMP_420
			JC_Turbojpeg *turbojpeg420 = JC_Turbojpeg_Create(widths[i], widths[j], JPEG_PIXEL_FORMAT, TJSAMP_420,
				JPEG_QUALITY);
			JC_Turbojpeg_Encode(turbojpeg420, buffer, &jpegBuffer, &jpegSize);
			char filename420[64];
			sprintf(filename420, "samples/jpeg-buffers/%dx%d-bgra-420-%d.jpg", widths[i], widths[j], JPEG_QUALITY);
			saveBuffer(filename420, jpegBuffer, jpegSize);

			char filename[64];
			sprintf(filename, "samples/buffers/%dx%d", widths[i], widths[j]);
			saveBuffer(filename, buffer, widths[i] * widths[j] * 4);

			free(buffer);
			JC_Turbojpeg_Destroy(turbojpeg444);
			JC_Turbojpeg_Destroy(turbojpeg422);
			JC_Turbojpeg_Destroy(turbojpeg420);
		}
	}
}
