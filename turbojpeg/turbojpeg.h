#ifndef JPEG_CODEC_TURBOJPEG_H
#define JPEG_CODEC_TURBOJPEG_H

#include <turbojpeg.h>

typedef struct {
	int width;
	int height;
	int pitch;
	int bufferSize;
	enum TJPF pixelFormat;
	enum TJSAMP subsample;
	int qualilty;
	tjhandle handleCompress;
	tjhandle handleDecompress;
} JC_Turbojpeg;

JC_Turbojpeg *JC_Turbojpeg_Create(int width, int height, enum TJPF pixelFormat, enum TJSAMP subsample,
	int quality);
void JC_Turbojpeg_Destroy(JC_Turbojpeg *turbojpeg);

void JC_Turbojpeg_Encode(JC_Turbojpeg *turbojpeg, unsigned char *buffer, unsigned char **jpegBuffer,
	unsigned long *jpegSize);
void JC_Turbojpeg_Decode(JC_Turbojpeg *turbojpeg, unsigned char *jpegBuffer, unsigned long jpegSize,
	unsigned char *buffer);

#endif
