#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <turbojpeg.h>

#include "turbojpeg.h"

JC_Turbojpeg *JC_Turbojpeg_Create(int width, int height, enum TJPF pixelFormat, enum TJSAMP subsample,
	int quality)
{
	JC_Turbojpeg *turbojpeg = malloc(sizeof (JC_Turbojpeg));
	assert(turbojpeg);

	turbojpeg->handleCompress = tjInitCompress();
	turbojpeg->handleDecompress = tjInitDecompress();

	turbojpeg->width = width;
	turbojpeg->height = height;
	turbojpeg->pitch = width * 4;
	turbojpeg->bufferSize = width * height * 4;
	turbojpeg->pixelFormat = pixelFormat;
	turbojpeg->subsample = subsample;
	turbojpeg->qualilty = quality;

	return turbojpeg;
}

void JC_Turbojpeg_Destroy(JC_Turbojpeg *turbojpeg)
{
	tjDestroy(turbojpeg->handleCompress);
	tjDestroy(turbojpeg->handleDecompress);
	free(turbojpeg);
}

void JC_Turbojpeg_Encode(JC_Turbojpeg *turbojpeg, unsigned char *buffer, unsigned char **jpegBuffer,
	unsigned long *jpegSize)
{
	tjCompress2(turbojpeg->handleCompress, buffer, turbojpeg->width, 0, turbojpeg->height, turbojpeg->pixelFormat,
		jpegBuffer, jpegSize, turbojpeg->subsample, turbojpeg->qualilty, 0);
}

void JC_Turbojpeg_Decode(JC_Turbojpeg *turbojpeg, unsigned char *jpegBuffer, unsigned long jpegSize,
	unsigned char *Buffer)
{
	tjDecompress2(turbojpeg->handleCompress, jpegBuffer, jpegSize, jpegBuffer, turbojpeg->width,
		turbojpeg->pitch, turbojpeg->height, turbojpeg->pixelFormat, 0);
}
