#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "jpeg-marker.h"
#include "jpeg-image.h"

int main(int argc, char **argv)
{
	if (argc != 2) {
		printf("USAGE: ./main <filename>\n");
		return 1;
	}
	JpegImage *jpegImage = JpegImage_Create(argv[1]);
	if (!jpegImage) {
		return 1;
	}
	printf("jpeg size %zu\n", jpegImage->size);
	JpegMarker_Read(jpegImage);
	printf("\n");
	return 0;
}
