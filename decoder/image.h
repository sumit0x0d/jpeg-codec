#ifndef JPEG_CODEC_IMAGE_H
#define JPEG_CODEC_IMAGE_H

#include <stddef.h>
#include <stdint.h>

typedef struct JpegImage {
	uint8_t *data;
	size_t size;
} JpegImage;

JpegImage *JpegImage_Create(const char* filename);
void JpegImage_Destroy(JpegImage *jpegImage);

#endif
