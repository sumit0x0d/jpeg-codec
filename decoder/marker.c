#include <stdio.h>

#include "marker.h"

static uint16_t readApp(uint8_t* byte);
static uint16_t readCom(uint8_t* byte);
static uint16_t readDqt(uint8_t* byte);
static uint16_t readSos(uint8_t* byte);

void JpegMarker_Read(JpegImage *jpegImage)
{
	for (size_t i = 2; i < jpegImage->size - 2; i++) {
		if (jpegImage->data[i] != 0xFF) {
			continue;
		}
		i++;
		if (jpegImage->data[i] >= APP0 && jpegImage->data[i] <= APP15) {
			i = readApp(jpegImage->data + i);
		} else if (jpegImage->data[i] == COM) {
			i = readCom(jpegImage->data + i);
		} else if (jpegImage->data[i] == DQT) {
			// i = readDqt(jpegImage->data + i);
		} else if (jpegImage->data[i] == DHT) {
			// readDqt(jpegImage->data + i);
		} else if (jpegImage->data[i] == SOS) {
			readSos(jpegImage->data + i);
		}
	}
}

static uint16_t readApp(uint8_t* byte)
{
	uint16_t index = 0;
	printf("JPEG Marker: APP (0xFF%02X)\n", byte[index]);
	index++;
	uint16_t length = (byte[index] >> 8) + byte[index + 1];
	index += 2;
	length -= 2;
	printf("JPEG Marker uint16_t: %d\n", length);
	printf("Jpeg Marker Data: ");
	while (length) {
		printf("%c", byte[index]);
		index++;
		length--;
	}
	printf("\n");
	return index - 1;
}

static uint16_t readCom(uint8_t* byte)
{
	uint16_t index = 0;
	printf("JPEG Marker: COM (0xFF%02X)\n", byte[index]);
	index++;
	uint16_t length = (byte[index] >> 8) + byte[index + 1];
	index += 2;
	length -= 2;
	printf("JPEG Marker uint16_t: %d\n", length);
	printf("Jpeg Marker Data: ");
	while (length) {
		printf("%c", byte[index]);
		index++;
		length--;
	}
	printf("\n");
	return index - 1;
}

static uint16_t readDqt(uint8_t* byte)
{
	uint16_t index = 0;
	printf("JPEG Marker: DQT (0xFF%02X)\n", byte[index]);
	index++;
	uint16_t length = (byte[index] >> 8) + byte[index + 1];
	index += 2;
	uint8_t table_info = byte[index] >> 4;
	if (table_info) {
		// 16 bits table
	} else {
		// 8 bits table
	}
	uint8_t table_id = byte[index] & 0x0F;
	/* if ((byte[index] >> 8) && 0xFF == 0x00) { */
		
	/* } else { */
		
	/* } */
	printf("JPEG 1st DQT: %d\n", byte[index]);	
	index++;
	printf("JPEG 2st DQT: %d\n", byte[index]);
	length -= 3;
	printf("JPEG Marker uint16_t: %d\n", length);
	printf("Table ID: %d\n", length);
	printf("Jpeg Marker Data: ");
	while (length) {
		printf("%c", byte[index]);
		index++;
		length--;
	}
	printf("\n");
	return index - 1;
}

static uint16_t readSos(uint8_t* byte)
{
	uint16_t index = 0;
	printf("JPEG Marker: SOS (0xFF%02X)\n", byte[index]);
	index++;
	uint16_t length = (byte[index] >> 8) + byte[index + 1];
	index += 2;
	printf("JPEG Marker uint16_t: %d\n", length);
	index++;
	printf("Jpeg Marker Data: ");
	index++;
	length -= 3;
	while (length) {
		printf("%c", byte[index]);
		index++;
		length--;
	}
	printf("\n");
	return index - 1;
}
