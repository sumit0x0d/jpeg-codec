#ifndef JPEG_CODEC_MARKER_H
#define JPEG_CODEC_MARKER_H

#include <stdint.h>

#include "jpeg-image.h"

// Start Of Frame markers, non-differential, Huffman coding
static const uint8_t SOF0 = 0xC0; // Start of Frame 0 | Baseline DCT
static const uint8_t SOF1 = 0xC1; // Start of Frame 1 | Extended sequential DCT
static const uint8_t SOF2 = 0xC2; // Start of Frame 2 | Progressive DCT
static const uint8_t SOF3 = 0xC3; // Start of Frame 3 | Lossless (sequential)

// Huffman table specification
static const uint8_t DHT = 0xC4; // Define Huffman table(s)

// Start Of Frame markers, differential, Huffman coding
static const uint8_t SOF5 = 0xC5; // Start of Frame 5 | Differential sequential DCT
static const uint8_t SOF6 = 0xC6; // Start of Frame 6 | Differential progressive DCT
static const uint8_t SOF7 = 0xC7; // Start of Frame 7 | Differential lossless (sequential)

// Start Of Frame markers, non-differential, arithmetic coding
static const uint8_t JPG = 0xC8; // Reserved for JPEG extensions
static const uint8_t SOF9 = 0xC9; // Start of Frame 9 | Extended sequential DCT
static const uint8_t SOF10 = 0xCA; // Start of Frame 10 | Progressive DCT
static const uint8_t SOF11 = 0xCB; // Start of Frame 11 | Lossless (sequential)

// Arithmetic coding conditioning specification
static const uint8_t DAC = 0xCC; // Define arithmetic coding conditioning(s)

//Start Of Frame markers, differential, arithmetic coding
static const uint8_t SOF13 = 0xCD; // Start of Frame 13 | Differential sequential DCT
static const uint8_t SOF14 = 0xCE; // Start of Frame 14 | Differential progressive DCT
static const uint8_t SOF15 = 0xCF; // Start of Frame 15 | Differential lossless (sequential)

// Restart interval termination
static const uint8_t RST0 = 0xD0; // Restart Marker 0
static const uint8_t RST1 = 0xD1; // Restart Marker 1
static const uint8_t RST2 = 0xD2; // Restart Marker 2
static const uint8_t RST3 = 0xD3; // Restart Marker 3
static const uint8_t RST4 = 0xD4; // Restart Marker 4
static const uint8_t RST5 = 0xD5; // Restart Marker 5
static const uint8_t RST6 = 0xD6; // Restart Marker 6
static const uint8_t RST7 = 0xD7; // Restart Marker 7

//Other markers
static const uint8_t SOI = 0xD8; // Start of image
static const uint8_t EOI = 0xD9; // End of image
static const uint8_t SOS = 0xDA; // Start of scan
static const uint8_t DQT = 0xDB; // Define quantization table(s)
static const uint8_t DNL = 0xDC; // Define number of lines
static const uint8_t DRI = 0xDD; // Define restart interval
static const uint8_t DHP = 0xDE; // Define hierarchical progression
static const uint8_t EXP = 0xDF; // Expand Reference Component(s)

// Reserved for application segments
static const uint8_t APP0 = 0xE0; // Application segment 0 | JFIF – JFIF JPEG image | AVI1 – Motion JPEG (MJPG)
static const uint8_t APP1 = 0xE1; // Application segment 1 | EXIF Metadata, TIFF IFD format, JPEG Thumbnail (160×120) | Adobe XMP
static const uint8_t APP2 = 0xE2; // Application segment 2 |
static const uint8_t APP3 = 0xE3; // Application segment 3 |
static const uint8_t APP4 = 0xE4; // Application segment 4 |
static const uint8_t APP5 = 0xE5; // Application segment 5 |
static const uint8_t APP6 = 0xE6; // Application segment 6 |
static const uint8_t APP7 = 0xE7; // Application segment 7 | Lossless JPEG
static const uint8_t APP8 = 0xE8; // Application segment 8 |
static const uint8_t APP9 = 0xE9; // Application segment 9 |
static const uint8_t APP10 = 0xEA; // Application segment 10 |
static const uint8_t APP11 = 0xEB; // Application segment 11 |
static const uint8_t APP12 = 0xEC; // Application segment 12 |
static const uint8_t APP13 = 0xED; // Application segment 13 |
static const uint8_t APP14 = 0xEE; // Application segment 14 |
static const uint8_t APP15 = 0xEF; // Application segment 15 |

// Reserved for JPEG extensions
static const uint8_t JPG0 = 0xF0;
static const uint8_t JPG1 = 0xF1;
static const uint8_t JPG2 = 0xF2;
static const uint8_t JPG3 = 0xF3;
static const uint8_t JPG4 = 0xF4;
static const uint8_t JPG5 = 0xF5;
static const uint8_t JPG6 = 0xF6;
static const uint8_t JPG7 = 0xF7;
static const uint8_t JPG8 = 0xF8;
static const uint8_t JPG9 = 0xF9;
static const uint8_t JPG10 = 0xFA;
static const uint8_t JPG11 = 0xFB;
static const uint8_t JPG12 = 0xFC;
static const uint8_t JPG13 = 0xFD;

static const uint8_t COM = 0xFE; // Comment
static const uint8_t PREFIX = 0xFF;
static const uint8_t TEM = 0x01;

void JpegMarker_Read(JpegImage *jpegImage);

#endif
