#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "jpeg-image.h"
#include "jpeg-marker.h"

JpegImage* JpegImage_Create(const char* filename)
{
	FILE* file = fopen(filename, "rb");
	assert(file);

	if (fgetc(file) == 0xFF && fgetc(file) == SOI) {
		printf("valid\n");
	} else {
		fclose(file);
		return NULL;
	}
	JpegImage* jpegImage = (JpegImage*)malloc(sizeof (JpegImage));
	assert(jpegImage);
	fseek(file, 0, SEEK_END);
	jpegImage->size = ftell(file);
	rewind(file);
	jpegImage->data = (uint8_t*)malloc(jpegImage->size);
	assert(jpegImage->data);
	fread(jpegImage->data, jpegImage->size, 1, file);
	fclose(file);
	return jpegImage;
}

void JpegImage_Destroy(JpegImage *jpegImage)
{
	free(jpegImage->data);
	free(jpegImage);
}
